<?php

namespace Drupal\archimedes_client\Item;

use Drupal\archimedes_client\Item;
use Drupal\Component\Utility\Html as HtmlUtil;

/**
 * Environment.
 *
 * The current environment, as set by the Environment module.
 *
 * @package Archimedes
 * @subpackage Client
 */
class Environment extends Item {

  /**
   * Gets an array of the current environment name and label.
   *
   * @return array
   *   Environment name and label
   */
  public function get() {
    // Prefer to retrieve this from environment_indicator.
    $config = \Drupal::config('environment_indicator.indicator');
    if ($name = $config->get('name')) {
      return ['Label' => $name, 'Name' => $name];
    }
    // Alternative in define.
    elseif (defined('MATAARA_ENVIRONMENT')) {
      return ['Label' => constant('MATAARA_ENVIRONMENT'), 'Name' => constant('MATAARA_ENVIRONMENT')];
    }
    elseif (getenv('MATAARA_ENVIRONMENT', TRUE)) {
      return [
        'Label' => getenv('MATAARA_ENVIRONMENT', TRUE),
        'Name' => getenv('MATAARA_ENVIRONMENT', TRUE),
      ];
    }

    return ['Label' => 'Unknown', 'Name' => 'unknown'];
  }

  /**
   * Gets a string denoting the current environment and its label.
   *
   * @return string
   *   HTML markup
   */
  public function render() {
    $environment = $this->get();
    return HtmlUtil::escape($environment['Label']);
  }

}
