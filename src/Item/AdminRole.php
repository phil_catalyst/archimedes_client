<?php

namespace Drupal\archimedes_client\Item;

use Drupal\archimedes_client\Item;
use Drupal\user\Entity\User;

/**
 * AdminRole.
 *
 * Determines the Administrator role and grabs some basic details of users that have it.
 *
 * @package Archimedes
 * @subpackage Client
 */
class AdminRole extends Item {

  /**
   * Gets a list of the administrator roles.
   *
   * @return array
   *   Role and user data
   */
  public function get() {
    // Get all administrator roles.
    $admin_role_ids = \Drupal::entityQuery('user_role')->condition('is_admin', TRUE)->execute();
    $admin_roles = \Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple($admin_role_ids);

    // Fetch the users in each administrator role.
    $roles = [];
    foreach ($admin_roles as $role) {
      // Fetch users for this role.
      $role_uids = \Drupal::entityQuery('user')->condition('roles', 'administrator')->accessCheck(FALSE)->execute();
      /*
       * $role_users is rendered as a JSON array for backwards compatibility;
       * the server will be able to detect legacy reports with single admin
       * roles more easily via type-checking.
       */
      $role_users = [];
      foreach ($role_uids as $uid) {
        $user = User::load($uid);
        $role_users[$uid] = [
          'Name'      => $user->name->value,
          'Mail'      => $user->mail->value,
          'Status'    => $user->status->value,
          'LastLogin' => $user->login->value
        ];
      }

      $roles[] = [
        'RoleName'  => $role->get('label'),
        'RoleID'    => $role->get('id'),
        'RoleUsers' => $role_users
      ];
    }

    return $roles;
  }

  /**
   * Gets a string denoting the number of nodes and revisions on the site.
   *
   * @return string
   *   HTML markup
   */
  public function render() {
    $roles = $this->get();
    $count = count($roles);

    if ($count > 0) {
      return t('There @isare @count administrator @word', [
        '@word' => ($count == 1) ? 'role' : 'roles',
        '@isare' => ($count == 1) ? 'is' : 'are',
        '@count' => $count,
      ]);
    }
    else {
      return 'No admin roles were found';
    }
  }

}
