<?php

namespace Drupal\archimedes_client\Item;

use Drupal\archimedes_client\Item;

/**
 * DirectoryHash.
 *
 * A hash of the files and folders in Drupal's root, to detect code changes.
 *
 * @package Archimedes
 * @subpackage Client
 */
class DirectoryHash extends Item {

  /**
   * Get the hash, given some file and folder exceptions.
   *
   * @return string
   *   Hash
   */
  public function get() {
    $ignore = ['#(\w*/)*sites/\w*/files#', '#.*\.(git|svn|bzr|cvs)/.*#'];
    return $this->directory_hash(DRUPAL_ROOT, $ignore);
  }

  /**
   * Calculates the MD5 hash of a directory by recursing through files.
   *
   * Symlinks are counted but not followed, to avoid entering an infinite loop.
   *
   * @param string $dir
   *   Directory to begin recursing through.
   * @param array $ignore
   *   An array of regex path exlusions, to be parsed by preg_match.
   *
   * @return string
   *   MD5 hash
   */
  private function directory_hash($dir, $ignore) {
    if (!is_dir($dir)) {
      return FALSE;
    }

    // Symlink count is important. While we don't want to follow
    // symlinks, we need to know they are there incase they are
    // introduced or removed.
    $symlinks = [];

    $filemd5s = [];
    $d = dir($dir);

    while (($entry = $d->read()) !== FALSE) {
      // Skip current/parent directory references.
      if (in_array($entry, ['.', '..'])) {
        continue;
      }

      // If the beginning of the path does not match exactly, or if
      // this is a symlink then this directory does not lead deeper
      // but to somewhere else which may create a recursive loop.
      $path = realpath($dir . '/' . $entry);
      if (strpos($path, $dir) !== 0) {
        $symlinks[] = $path;
        continue;
      }

      // Ignore any entries that match the preg filters.
      $ignore_entry = FALSE;
      foreach ($ignore as $pattern) {
        if (preg_match($pattern, $path)) {
          $ignore_entry = TRUE;
          break;
        }
      }
      if ($ignore_entry) {
        continue;
      }

      // Recurse into directories.
      if (is_dir($path)) {
        $filemd5s[] = $this->directory_hash($path, $ignore);
      }
      elseif (is_file($path)) {
        $filemd5s[] = md5_file($path);
      }
    }

    $d->close();

    // Sort the md5s before concat so ensure order of files doesn't affect it.
    asort($filemd5s);

    return md5(implode('', $filemd5s) . implode('', $symlinks));
  }

}
