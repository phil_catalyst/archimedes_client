<?php

namespace Drupal\archimedes_client;

/**
 * Item.
 *
 * An item to be included in an Archimedes Report.
 *
 * @package Archimedes
 * @subpackage Client
 */
abstract class Item {

  /**
   * Fetch item data.
   *
   * @return mixed
   *   Some value or data structure representing the item.
   */
  abstract public function get();

  /**
   * Render a string representation of item data.
   *
   * This function can be overridden by concrete ArchimedesItem classes.
   * NB: unit tests will fail if this does not return a string.
   *
   * @return string
   *   A human-readable string representation of get() data.
   *
   * @see ArchimedesItem::get()
   */
  public function render() {
    return "{$this->get()}";
  }

}
