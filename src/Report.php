<?php

namespace Drupal\archimedes_client;

use Exception;

/**
 * Report.
 *
 * A report to be prepared and sent to an Archimedes server endpoint.
 *
 * @package Archimedes
 * @subpackage Client
 */
class Report {
  /**
   * Loaded ArchimedesItem classes.
   *
   * @var array
   */
  private $items = [];

  /**
   * A list of the rendered report items.
   *
   * @var array
   */
  private $rendered;

  /**
   * A list of the report item machine values.
   *
   * @var array
   */
  private $data;

  /**
   * Base64 encoded string of the encrypted report data.
   *
   * @var string
   */
  public $encrypted = '';

  /**
   * Bas64 encoded string of the encrypted ekey.
   *
   * @var string
   */
  public $ekey = '';

  /**
   * Loads item classes upon constructions.
   */
  public function __construct() {
    // Dynamically populate with available report item types.
    $this->createItems();
  }

  /**
   * Dynamically loads an array of report item classes.
   *
   * Fetches classes from the '/lib/items' subdirectory. All item classes should
   * extend from the ArchimedesItem class.
   *
   * The 'archimedes_load_item_classfiles' invocation expects that zero or more
   * modules will return an array of individual arrays with the following keys:
   * - module : the name of the calling module
   * - subdir : a subdirectory where ArchimedesItem class files can be found
   * - files : an array of file hooks from file_scan_directory()
   *
   * NB: I don't know if there's implicit ordering for when hooks get called,
   * so modules may override each other's definitions in arbitrary order on a
   * "last come, only served" basis.
   *
   * @TODO Extracting this array out into a separate class could be worthwhile.
   */
  private function createItems() {
    // Iterate over each file in the directory.
    $files = \Drupal::service('file_system')->scanDirectory(DRUPAL_ROOT . '/' . \Drupal::service('extension.path.resolver')->getPath('module', 'archimedes_client') . '/src/Item', '/\.php/');

    foreach ($files as $file) {
      // Try to load the class based on the filename.
      $class = $file->name;
      $class_namespaced = '\Drupal\archimedes_client\Item\\' . $class;

      if (class_exists($class_namespaced)) {
        $obj = new $class_namespaced();
        if ($obj instanceof Item) {
          $this->items[$class] = $obj;
        }
        else {
          \Drupal::logger('archimedes_client')->warning("Item defined in '$class.php' doesn't extend \Drupal\archimedes_client\Item");
        }
      }
      else {
        \Drupal::logger('archimedes_client')->warning("Item defined in '$class.php' can't be loaded by class name");
      }
    }

    // $external_item_classes = module_invoke_all('archimedes_load_item_classfiles', $this);
    //
    // // TODO: Refactor this so that they're blended in with the initial class files array.
    // foreach ($external_item_classes as $path => $item_class_mixin)
    // {
    //   foreach($item_class_mixin['files'] as $file)
    //   {
    //     // Try to import class file
    //     module_load_include('inc', $item_class_mixin['module'], $item_class_mixin['subdir'] . $file->name);
    //
    //     // Try to load the class based on the filename
    //     $class_name = $file->name . 'Item';
    //     $newItem = new $class_name();
    //
    //     if (class_exists($class_name) && $newItem instanceof ArchimedesItem)
    //       $this->items[$file->name] = $newItem;
    //     else
    //       watchdog('archimedes_client', "Unable to load item class '$class_name' for '".$item_class_mixin['module']."' from '".$item_class_mixin['subdir']."$file->name.inc'", array(), WATCHDOG_WARNING);
    //
    //
    //   }
    // }

    // Sort the list by class name.
    ksort($this->items);
  }

  /**
   * Fetches the raw array of items in the report.
   *
   * @return array
   */
  public function getArray() {
    // Check to see if this instance has already been fetched as an array.
    if (is_array($this->data)) {
      return $this->data;
    }

    // Call the get() method of each installed Item class.
    $this->data = [];
    foreach ($this->items as $name => $obj) {
      $this->data[$name] = $obj->get();
    }
    return $this->data;
  }

  /**
   * Fetches a JSON representation of the report.
   *
   * @param bool $pretty
   *   (optional) output pretty-printed (indented) JSON.
   *
   * @return string
   */
  public function getJSON($pretty = FALSE) {
    $options = 0;
    if ($pretty) {
      // The JSON_PRETTY_PRINT option is only available in PHP 5.4+.
      if (FALSE && defined('JSON_PRETTY_PRINT')) {
        $options |= JSON_PRETTY_PRINT;
      }
      else {
        // Fallback to librrary function for pretty-printing.
        return $this->jsonpp(json_encode($this->getArray()), '    ');
      }
    }
    return json_encode($this->getArray(), $options);
  }

  /**
   * Fetch the OpenSSL Encrypted version of the report.
   *
   * @return string
   *   A Base64 encoded representation of the encrypted report data
   */
  public function getEncrypted() {
    // Load the public key.
    $pubkey_data = \Drupal::config('archimedes_client.settings')->get('crypto.pubkey');
    $pubkey = openssl_pkey_get_public($pubkey_data);

    if ($pubkey === FALSE) {
      $err_msg = 'Error reading public key: ' . openssl_error_string();
      throw new Exception($err_msg);
    }

    // Seal the data, generating ekeys (one per key)
    openssl_seal($this->getJSON(), $sealed, $ekeys, [$pubkey], 'RC4');

    // Set attributes - take the 0th ekey (we only gave one pubkey)
    $this->encrypted = base64_encode($sealed);
    $this->ekey = base64_encode($ekeys[0]);

    return $this->encrypted;
  }

  /**
   * Fetch the OpenSSL envelope key for the encrypted report.
   *
   * @return string
   *   A based64-encoded version of binary key data.
   */
  public function getEkey() {
    if (empty($this->ekey)) {
      $this->getEncrypted();
    }
    return $this->ekey;
  }

  /**
   * Fetch rendered string representations of all report items.
   *
   * Equivalent to mapping render() onto each value returned by getArray().
   *
   * @return array
   *   An array of string-rendered ArchimedesItem objects.
   */
  public function getRendered() {
    // Check to see if this instance has already been rendered.
    if (is_array($this->rendered)) {
      return $this->rendered;
    }

    // Call the render() methods (which fallback to the get() methods)
    $this->rendered = [];
    foreach ($this->items as $name => $obj) {
      $this->rendered[$name] = $obj->render();
    }
    return $this->rendered;
  }

  /**
   * Send a report to the configured server endpoint.
   *
   * Updated the 'archimedes_client_last_report_sent' variable only if the
   * sendHTTP() or sendEmail() function returns a true result.
   *
   * @param string $method
   *   Override the reporting method to be used.
   * @param string $location
   *   Override the endpoint location (e.g. Email or URL)
   */
  public function send($method = NULL, $location = NULL) {
    if (!$method) {
      $method = \Drupal::config('archimedes_client.settings')->get('server.method');
    }
    switch ($method) {
      case 'http':
        $sendResult = $this->sendHTTP($location);

        if ($sendResult) {
          \Drupal::state()->set('archimedes_client.last_report', \Drupal::time()->getRequestTime());
        }

        return $sendResult;

      break;

      case 'email':
      default:
        $sendResult = $this->sendEmail($location);

        if ($sendResult) {
          \Drupal::state()->set('archimedes_client.last_report', \Drupal::time()->getRequestTime());
        }

        return $sendResult;

      break;
    }
  }

  /**
   * Regenerate the report data.
   *
   * Invalidates existing cached item data, allowing new data to be generated.
   */
  public function regenerate() {
    // Setting rendered/data to NULL forces ReportItem classes to be queried again.
    $this->rendered = NULL;
    $this->data = NULL;
  }

  /**
   * Send a report to an HTTP endpoint.
   *
   * @param string $url
   *   Override the configured endpoint URL.
   */
  private function sendHTTP($url = NULL) {
    // Fetcht the configured URL if not passed.
    if (!$url) {
      $url = \Drupal::config('archimedes_client.settings')->get('server.url');
    }

    // Prepare POST variables.
    try {
      $data = $this->getEncrypted();
      $ekey = $this->getEkey();
    }
    catch (Exception $e) {
      return $e->getMessage();
    }

    $post = ['ek' => $ekey, 'enc' => $data];

    // Initiate & make an HTTP request using CURL.
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($ch);

    // Check the status code is 200 (OK)
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if ($status && $status != 200) {
      return "CURL error: Response code was $status";
    }

    // Catch any CURL errors.
    if (curl_errno($ch)) {
      return 'CURL error: ' . curl_error($ch);
    }

    curl_close($ch);

    // Decode the JSON response.
    $data = json_decode($result);
    if (json_last_error() != JSON_ERROR_NONE || !isset($data->success)) {
      return 'Unable to decode JSON response';
    }

    // Return TRUE or an error string.
    if ($data->success !== TRUE) {
      return 'Response was: ' . $data->error;
    }
    else {
      return TRUE;
    }
  }

  /**
   * Send a report to an Email endpoint.
   *
   * @param string $email
   *   Override the configured endpoint email address.
   */
  private function sendEmail($email = NULL) {
    // Fetcht the configured URL if not passed.
    if (!$email) {
      $email = \Drupal::config('archimedes_client.settings')->get('server.email');
    }

    // Get encrypted data for attaching.
    try {
      $data = $this->getEncrypted();
      $ekey = $this->getEkey();
    }
    catch (Exception $e) {
      return $e->getMessage();
    }

    // Construct MIME multipart message
    //  - based on: http://stackoverflow.com/questions/12301358/send-attachments-with-php-mail
    $separator = md5(microtime());
    $eol = PHP_EOL;

    // Headers.
    $headers  = "MIME-Version: 1.0" . $eol;
    $headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"" . $eol;
    $headers .= "Content-Transfer-Encoding: 7BIT" . $eol;

    // Message.
    $message  = "--" . $separator . $eol;
    $message .= "Content-Type: text/plain" . $eol;
    $message .= "Content-Transfer-Encoding: 7BIT" . $eol . $eol;
    $message .= "Encrypted update attached." . $eol . $eol;

    // Attachments.
    $message .= "--" . $separator . $eol;
    $message .= "Content-Type: application/ekey; name=\"ekey.enc\"" . $eol;
    $message .= "Content-Disposition: attachment; filename=\"ekey.enc\"" . $eol;
    $message .= "Content-Transfer-Encoding: BASE64" . $eol . $eol;
    $message .= chunk_split((string) $ekey) . $eol;

    $message .= "--" . $separator . $eol;
    $message .= "Content-Type: application/json; name=\"data.enc\"" . $eol;
    $message .= "Content-Disposition: attachment; filename=\"data.enc\"" . $eol;
    $message .= "Content-Transfer-Encoding: BASE64" . $eol . $eol;
    $message .= chunk_split((string) $data) . $eol;

    // End.
    $message .= "--" . $separator . $eol;

    // Send the message using PHP's mail()
    return mail($email, 'Archimedes Update', $message, $headers);
  }

  /**
   * Jsonpp - Pretty print JSON data.
   *
   * In versions of PHP < 5.4.x, the json_encode() function does not yet provide a
   * pretty-print option. In lieu of forgoing the feature, an additional call can
   * be made to this function, passing in JSON text, and (optionally) a string to
   * be used for indentation.
   *
   * @param string $json
   *   The JSON data, pre-encoded.
   * @param string $istr
   *   The indentation string
   *
   * @link   https://github.com/ryanuber/projects/blob/master/PHP/JSON/jsonpp.php.
   *
   * @return string
   */
  public function jsonpp($json, $istr = ' ') {
    $result = '';
    for ($p = $q = $i = 0; isset($json[$p]); $p++) {
      $json[$p] == '"' && ($p > 0 ? $json[$p - 1] : '') != '\\' && $q = !$q;
      if (!$q && strchr(" \t\n\r", $json[$p])) {
        continue;
      }
      if (strchr('}]', $json[$p]) && !$q && $i--) {
        strchr('{[', $json[$p - 1]) || $result .= "\n" . str_repeat($istr, $i);
      }
      $result .= $json[$p];
      if (strchr(',{[', $json[$p]) && !$q) {
        $i += strchr('{[', $json[$p]) === FALSE ? 0 : 1;
        strchr('}]', $json[$p + 1]) || $result .= "\n" . str_repeat($istr, $i);
      }
    }
    return $result;
  }

}
