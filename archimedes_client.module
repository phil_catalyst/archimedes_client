<?php

/**
 * @file
 * Archimedes Client module, for reporting to an Archimedes server.
 */

use Drupal\archimedes_client\Report;
use Drupal\Core\Url;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\archimedes_client\Item\SiteKey;

/**
 * Implements hook_help().
 */
function archimedes_client_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case "help.page.archimedes_client":
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Archimedes is a site management tool for Drupal.') . '</p>';
      $output .= '<p>' . t('The <i>Archimedes Client</i> module regularly reports site, module and security information to an <i>Archimedes</i> server instance.') . '</p>';
      $output .= '<p>' . t('An Archimedes <i>report</i> collects a variety of potentially useful metadata about a website, including:') . '</p>';
      $output .= '<ul>';
      $output .= '<li>' . t('The Drupal core version') . '</li>';
      $output .= '<li>' . t('Environment descriptors (e.g. development, production)') . '</li>';
      $output .= '<li>' . t('Installed modules and their versions') . '</li>';
      $output .= '<li>' . t('The number of registered users and content nodes') . '</li>';
      $output .= '</ul>';
      $output .= '<p>' . t('Archimedes reports are sent in an encrypted format. The Archimedes Client module must have a public key in its <a href=":settings">settings</a>, and that public key must match a private key held on the server in order for the report to be accepted.', [':settings' => Url::fromRoute('archimedes_client.adminSettings')->toString()]) . '</p>';
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Send reports via HTTP') . '</dt>';
      $output .= '<dd>' . t('Archimedes can send reports securely via an HTTP web connection. The <i>Reporting Method</i> should be set to "<i>HTTP</i>", and the <i>Server URL</i> should point to a Archimedes HTTP Endpoint on the server, e.g. <i>http://www.examples.com/archimedes/endpoint</i>') . '</dd>';
      $output .= '<dt>' . t('Send reports via email') . '</dt>';
      $output .= '<dd>' . t('Archimedes can send reports securely via email. The <i>Reporting Method</i> should be set to "<i>Email</i>" and the <i>Server email address</i> should point to an address monitored by an Archimedes server, e.g. <i>archimedes@example.com</i>') . '</dd>';
      $output .= '<dt>' . t('Check report content') . '</dt>';
      $output .= '<dd>' . t('A summary of the site\'s Archimedes report can be viewed on the <a href=":status">status</a> page. A detailed JSON dump of the report content is also available on that page.', [':status' => Url::fromRoute('archimedes_client.adminStatus')->toString()]) . '</dd>';
      $output .= '</dl>';
      $output .= '<h3>' . t('Requirements') . '</h3>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Drupal 7') . '</dt>';
      $output .= '<dd>' . t('Archimedes has been developed and tested for use with Drupal 7 and PHP 5.3.x+.') . '</dd>';
      $output .= '<dt>' . t('Encryption keys') . '</dt>';
      $output .= '<dd>' . t('Archimedes uses public key encryption provided by PHP\'s <a href=":openssl">OpenSSL</a> extension.', [':openssl' => Url::fromUri('http://php.net/manual/en/book.openssl.php')->toString()]) . '</dd>';
      $output .= '</dl>';
      return $output;

    break;
  }
}

/**
 * Implements hook_cron().
 */
function archimedes_client_cron() {
  $config = \Drupal::config('archimedes_client.settings');

  // Runs the Archimedes cron tasks at a configured interval. Note that cron
  // won't run at exactly this time, but on the first cron run after it.
  $last_run = \Drupal::state()->get('archimedes_client.last_report', 0);
  $next_run = $last_run + $config->get('cron.interval');
  if ($last_run == 0 || \Drupal::time()->getRequestTime() >= $next_run) {
    $report = new Report();
    $r = $report->send();

    if ($r !== TRUE) {
      \Drupal::logger('archimedes_client')->error('Could not send report using @method method. Reason: %error_msg',
        [
          '@method' => $config->get('server.method'),
          '%error_msg' => $r
        ]);
    }
    else {
      \Drupal::logger('archimedes_client')->notice('Report sent using @method method (request via cron).',
        [
          '@method' => $config->get('server.method')
        ]);
    }
  }
}

/**
 * Implements hook_requirements().
 */
function archimedes_client_requirements($phase) {
  $requirements = [];
  $requirements['archimedes_client']['title'] = t('Archimedes Client');
  $message = NULL;

  if ($phase === 'runtime') {
    $config = \Drupal::config('archimedes_client.settings');
    $key = $config->get('report.key');

    if (empty($key)) {
      // Warn on empty keys.
      $message = t("No SiteKey is set! Please ensure there is a valid '%key' value in the '%settings' configuration object.",
      [
        '%key' => 'report.key',
        '%settings' => 'archimedes_client.settings'
      ]);
    }
    elseif (!preg_match('/^[a-f0-9]{32}$/', $key)) {
      // Warn on invalid MD5 keys.
      $message = t("Invalid MD5 key! Please ensure the '%key' value in '%settings' is a valid 32 byte MD5 hash.",
      [
        '%key' => 'report.key',
        '%settings' => 'archimedes_client.settings'
      ]);
    }

    if (!empty($message)) {
      $requirements['archimedes_client']['description'] = $message;
      $requirements['archimedes_client']['severity'] = REQUIREMENT_ERROR;
      $requirements['archimedes_client']['value'] = t('Valid site key needed');
    }
    else {
      $requirements['archimedes_client']['description'] = "Reporting with key: $key";
      $requirements['archimedes_client']['severity'] = REQUIREMENT_OK;
      $requirements['archimedes_client']['value'] = t('Configured');
    }

  }

  return $requirements;
}
