# Mataara Drupal Client

A module for Drupal that communicates site, module, and security information to a Mataara server instance.

Please note that the source code and other documentation in this repository still refers to the project by its previous name of Archimedes.

Uses standard D8 coding standards. Use Coder and phpcs to check your code: https://www.drupal.org/docs/8/modules/code-review-module/installing-coder-sniffer

Then:

  $ phpcs --standard=Drupal archimedes_client.module src
